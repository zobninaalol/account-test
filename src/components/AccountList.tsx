import React from 'react';
import Account from "../Account";
import {AccountItem} from "./AccountItem";

interface AccountListProps {
    accountList: Array<Account>
}

export const AccountList = (props: AccountListProps) => {
    const {accountList} = props;
    return (
        <div>
            {
                accountList.map(account => {
                    return <AccountItem key={`account_${account.id}`} account={account}/>
                })
            }
        </div>
    )
};