import React from 'react';
import Account from "../Account";

interface AccountItemProps {
    account: Account
}

export const AccountItem = (props: AccountItemProps) => {
    const {account} = props;
    return (
        <div>
            {account.id} | {account.name} | {account.balance}
        </div>
    );
};
