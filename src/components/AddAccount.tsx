import React, {Component} from "react";
import Account from "../Account";

interface AddAccountProps {
    id: number,
    name: string,
    balance: number,
    submitMethod: (account: Account) => void
}

export default class AddAccount extends Component<any, AddAccountProps> {
    constructor(props: AddAccountProps) {
        super(props);
        this.state = {
            id: 0,
            name: '',
            balance: 0,
            submitMethod: props.submitMethod
        };

        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeBalance = this.handleChangeBalance.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeId(event: any) {
        this.setState({id: event.target.value})
    }

    handleChangeName(event: any) {
        this.setState({name: event.target.value})
    }

    handleChangeBalance(event: any) {
        this.setState({balance: event.target.value})
    }

    handleSubmit(event: any){
        //event.preventDefault();
        const {id, name, balance} = this.state;
        this.state.submitMethod(new Account(id, name, balance));
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const {id, name, balance} = this.state;

        return (
            <div>
                <form>
                    <p>
                        <label htmlFor="id">
                            Id:
                            <input type="number" value={id} id="id" onChange={this.handleChangeId}/>
                        </label>
                    </p>
                    <p>
                        <label htmlFor="name">
                            Name:
                            <input type="text" value={name} id="name" onChange={this.handleChangeName}/>
                        </label>
                    </p>
                    <p>
                        <label htmlFor="balance">
                            Balance:
                            <input type="number" value={balance} id="balance" onChange={this.handleChangeBalance}/>
                        </label>
                    </p>
                    <p>
                        <button type={"button"} onClick={this.handleSubmit}>Add</button>
                    </p>
                </form>
            </div>
        )
    }

}