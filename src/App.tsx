import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Link,
    RouteComponentProps,
} from "react-router-dom";
import {AccountList} from "./components/AccountList";
import AddAccount from "./components/AddAccount";
import Account from "./Account";

interface AppProps {
    accountList: Array<Account>
}

class App extends Component<any, AppProps> {
    constructor(props: AppProps) {
        super(props);
        this.state = {
            accountList: props.accountList
        };

        this.handleCreateAccount = this.handleCreateAccount.bind(this);
    }

    handleCreateAccount(account: Account) {
        console.log(account);
        let {accountList} = this.state;
        accountList.push(account);
        this.setState({accountList: accountList});
    }

    render(): React.ReactElement | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {

        const {accountList} = this.state;
        return (
            <div className="App">
                <header>
                    <Router>
                        <div>
                            <nav>
                                <ul>
                                    <li>
                                        <Link to="/">Home</Link>
                                    </li>
                                    <li>
                                        <Link to="/accounts">Accounts</Link>
                                    </li>
                                    <li>
                                        <Link to="/accounts/add">Add Account</Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <Route path="/" exact component={Index}/>
                        <Route exact path="/accounts/add"
                               render={(props) => <AddAccount submitMethod={this.handleCreateAccount}/>}/>
                        <Route exact path="/accounts"
                               render={(props) => <AccountList accountList={accountList}/>}/>
                        {/*
                        //Может пригодиться
                        <Route path="/products/:id" component={Product}/>*/}
                    </Router>
                </header>
            </div>
        );
    }
}

function Index() {
    return <h2>Home</h2>;
}

/*
//Может пригодиться

type TParams = { id: string };

function Product({match}: RouteComponentProps<TParams>) {
    return <h2>This is a page for product with ID: {match.params.id} </h2>;
}
 */

export default App;
