import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Account from "./Account";

const defaultAccountList: Array<Account> = [
    new Account(1, 'account1', 25.00),
    new Account(2, 'account2', 50.75),
    new Account(3, 'account3', 43.75),
];

ReactDOM.render(
    <React.StrictMode>
        <App accountList={defaultAccountList}/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
